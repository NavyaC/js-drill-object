const data = require('./data.cjs');

function keys(data) {
    let arr = [];
    if (typeof (data) !== 'object' || keys.arguments.length !== 1) {
        return [];
    }
    for(let key in data) {
        arr.push(key);
    }
    return arr;
}

module.exports = keys;
