const data = require('./data.cjs');

function mapObject(data, cb) {
    const result = {};
    if (typeof (data) !== 'object' || mapObject.arguments.length < 2) {
        return {};
    }
    for(let key in data) {
        result[key] = cb(data[key]);
    }
    return result;
}


module.exports = mapObject;
