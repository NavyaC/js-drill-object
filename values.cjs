const data = require('./data.cjs');

function values(data) {
    let arr = [];
    if (typeof (data) !== 'object' || values.arguments.length !== 1) {
        return [];
    }
    for(let value in data) {
        arr.push(data[value]);
    }
    return arr;
}

module.exports = values;
