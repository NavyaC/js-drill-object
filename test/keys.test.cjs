const data = require('../data.cjs');

const keysFn = require('../keys.cjs');

test('Testing keys problem', () => {
    expect(keysFn(data)).toEqual(Object.keysFn(data));
});
