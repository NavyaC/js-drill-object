const data = require('../data.cjs');

const valuesFn = require('../values.cjs');

test('Testing values problem', () => {
    expect(valuesFn(data)).toEqual(Object.values(data));
});
