const data = require('../data.cjs');

const pairs = require('../pairs.cjs');

test('Testing pairs problem', () => {
    expect(pairs(data)).toEqual(Object.entries(data));
});
