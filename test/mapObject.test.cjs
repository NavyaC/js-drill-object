const data = require('../data.cjs');

const map = require('../mapObject.cjs');


function cb(value) {
    return value + 2;
}

const result = {name: "Bruce Wayne2", age: 38, location: "Gotham2"};

test('Testing mapObject problem', () => {
    expect(map(data, cb)).toEqual(result);
})
