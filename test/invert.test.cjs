const data = require('../data.cjs');

const invertFn = require('../invert.cjs');


const flipped = Object.entries(data).map(([key, value]) => [value, key]);

test('Testing invert problem', () => {
    expect(invertFn(data)).toEqual(Object.fromEntries(flipped));
})
