const data = require('../data.cjs');

const defaultsFn = require('../defaults.cjs');

const defaultProps = { flavor: "blackcurrent", sprinkles: "more" };

const result = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    flavor: 'blackcurrent',
    sprinkles: 'more'
}

test('Testing default problem', () => {
    expect(defaultsFn(data, defaultProps)).toEqual(result);
})
