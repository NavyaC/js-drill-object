const data = require('./data.cjs');

function pairs(data) {
    const result = [];
    if (typeof (data) !== 'object' || pairs.arguments.length !== 1) {
        return [];
    }
    for (let key in data) {
        result.push([key, data[key]]);
    }
    return result;
}

module.exports = pairs;
