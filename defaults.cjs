const data = require('./data.cjs');

function defaults(data, defaultProps) {
    if (typeof(data) !== 'object' || typeof(defaultProps) !== 'object' || defaults.arguments.length < 2) {
        return {};
    }
    for(let key in defaultProps) {
        if (data[key] === undefined) {
            data[key] = defaultProps[key];
        }
    }
    return data;
}

module.exports = defaults;
