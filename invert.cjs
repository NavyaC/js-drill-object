const data = require('./data.cjs');

function invert(data) {
    const result = {};
    if (typeof (data) !== 'object' || invert.arguments.length !== 1) {
        return {};
    }
    for(let key in data) {
        result[data[key]] = key;
    }
    return result;
}

module.exports = invert;
